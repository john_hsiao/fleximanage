FROM node:10-slim

RUN mkdir -p /var/log/flexiwan/manage/backend

RUN mkdir -p /var/log/flexiwan/manage/client

COPY ./backend /opt/flexiwan/manage/backend
COPY ./client /opt/flexiwan/manage/client

WORKDIR /opt/flexiwan/manage/backend

RUN npm install

EXPOSE 3000
EXPOSE 443

CMD node ./bin/www production