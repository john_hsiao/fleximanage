#! /bin/bash
date=$(date '+%Y%m%d')
sleep 15
declare -a backup_folders=("flexiwan")
for backup in "${backup_folders[@]}"
do
   echo "Bulid $backup"
   mongorestore -h mongo1 --port 27017 -d $backup /docker-entrypoint-initdb.d/$backup
done

declare -a backup_ana_folders=("flexiwanAnalytics")
for backup_ana in "${backup_ana_folders[@]}"
do
   echo "Bulid $backup_ana"
   mongorestore -h mongo1 --port 27017 -d $backup_ana /docker-entrypoint-initdb.d/$backup_ana
done

# echo "Add user to access db argi_dev"
# mongo "argi_dev" -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin /docker-entrypoint-initdb.d/addUser.js

# echo "Add user to access db authdb"
# mongo "authdb" -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin /docker-entrypoint-initdb.d/addAuthUser.js